/*

 Ridge - A high-throughput, low-latency amcast library
 Copyright (C) 2014, University of Lugano
 
 This file is part of Ridge.
 
 Ridge is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.ridge;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.netwrapper.tcp.TCPDestination;

import java.util.ArrayList;
import java.util.List;

public class MulticastAgent {
    static boolean conservativeDelivery = true;
    static boolean optimisticDelivery = false;
    static boolean fastDelivery = false;
    static boolean directFast = false;
    Process runningProcess;

    public MulticastAgent(Process runningProcess) {
        this.runningProcess = runningProcess;
    }

    public static void setConservative(boolean flag) {
        conservativeDelivery = flag;
    }

    public static void setOptimistic(boolean flag) {
        optimisticDelivery = flag;
    }

    public static void setFast(boolean flag) {
        fastDelivery = flag;
    }

    public static void setDirectFast(boolean flag) {
        directFast = flag;
    }

    public void multicast(RidgeMessage m, Ensemble destination) {
        m.ensembleId = destination.ensembleId;
        if (fastDelivery) {
            if (directFast) {
                sendToAllLearnersDirect(destination, m);
            } else {
                Learner contactLearner = destination.getBroadcaster(m.id.pid);
                runningProcess.send(m, contactLearner);
            }
        }

        if (conservativeDelivery || optimisticDelivery) {
            runningProcess.send(m, destination.coordinator);
        }
    }

    public void sendToAllLearnersDirect(Ensemble e, Message m) {
        List<TCPDestination> learners = new ArrayList<TCPDestination>(e.learnersList);
        runningProcess.send(m, learners);
    }

}
