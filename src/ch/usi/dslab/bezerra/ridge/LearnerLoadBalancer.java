package ch.usi.dslab.bezerra.ridge;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LearnerLoadBalancer {

   private static class LearnerLoad {
      public Learner learner;
      public long value;
      public LearnerLoad(Learner l) {
         learner = l;
         value   = 0l;
      }
   }

   Map<Learner, LearnerLoad> learnersLoadMap;
   
   public LearnerLoadBalancer() {
      learnersLoadMap = new HashMap<Learner, LearnerLoad>();
   }

   Learner setBroadcastingLearner(RidgeMessage batch, Ensemble e) {
      LearnerLoad lowestLoad = null;
      
      List<Learner> learners = e.getLearners();
      long min_load = Long.MAX_VALUE;
      for (Learner learner : learners) {
         LearnerLoad load = learnersLoadMap.get(learner);
         if (load == null) {
            load = new LearnerLoad(learner);
            learnersLoadMap.put(learner, load);
         }
         if (load.value < min_load) {
            min_load = load.value;
            lowestLoad = load;
         }
      }
      
      batch.setBroadcastingLearnerId(lowestLoad.learner.pid);      
      lowestLoad.value += batch.getBatchSize();

      return lowestLoad.learner;
   }
}
