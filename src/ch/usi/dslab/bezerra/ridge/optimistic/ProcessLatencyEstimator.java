package ch.usi.dslab.bezerra.ridge.optimistic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

public class ProcessLatencyEstimator {
   static int globalSampleSize = 5;
   static double globalNumDevs = 0;
   static int globalMaxEstimatedLatency = Integer.MAX_VALUE;
   private static List<ProcessLatencyEstimator> allInstances = Collections.synchronizedList(new ArrayList<ProcessLatencyEstimator>());
   
   public static void setGlobalSampleSize(int globalSampleSize) {
      ProcessLatencyEstimator.globalSampleSize = globalSampleSize;
      for (ProcessLatencyEstimator ple : allInstances)
         ple.setSampleSize(globalSampleSize);
   }
   
   public static void setGlobalNumDevs(double globalNumDevs) {
      ProcessLatencyEstimator.globalNumDevs = globalNumDevs;
      for (ProcessLatencyEstimator ple : allInstances)
         ple.setNumDevs(globalNumDevs);
   }
   
   public static void setGlobalMaxEstimatedLatency(int globalMaxEstimatedLatency) {
      ProcessLatencyEstimator.globalMaxEstimatedLatency = globalMaxEstimatedLatency;
      for (ProcessLatencyEstimator ple : allInstances)
         ple.setMaxEstimatedLatency(globalMaxEstimatedLatency);
   }
   
   public ProcessLatencyEstimator() {
      statistics = new DescriptiveStatistics(globalSampleSize);
      numDevs = globalNumDevs;
      maxEstimatedLatency = globalMaxEstimatedLatency;
      allInstances.add(this);
   }
   
   DescriptiveStatistics statistics;
   private double numDevs;
   private int maxEstimatedLatency;
   double averageCache = 0.0d;
   double standardDeviationCache = 0.0d;
   boolean staleAverageCache = false;
   boolean staleDeviationCache = false;

   public void setSampleSize(int sampleSize) {
      statistics.setWindowSize(sampleSize);
   }
   
   public void setNumDevs(double numDevs) {
      this.numDevs = numDevs;
   }
   
   public void setMaxEstimatedLatency(int maxEstimatedLatency) {
      this.maxEstimatedLatency = maxEstimatedLatency;
   }
   
   public void addLatency(long value) {
      statistics.addValue((double) value);
      staleAverageCache = staleDeviationCache = true;
   }

   public double getEstimatedLatency() {
      if (staleAverageCache) {
         averageCache = statistics.getMean();
         staleAverageCache = false;
      }
      
      double estimatedLatency;
      if (Math.abs(numDevs) < 0.0001d)
         estimatedLatency = averageCache;
      else 
         estimatedLatency = averageCache + numDevs * getStandardDeviation();
      
      return (estimatedLatency <= maxEstimatedLatency) ? estimatedLatency : maxEstimatedLatency;
   }

   public double getStandardDeviation() {
      if (staleDeviationCache) {
         standardDeviationCache = statistics.getStandardDeviation();
         staleDeviationCache = false;
      }
      return standardDeviationCache;
   }
}
