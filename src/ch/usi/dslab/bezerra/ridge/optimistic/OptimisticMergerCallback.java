package ch.usi.dslab.bezerra.ridge.optimistic;

import ch.usi.dslab.bezerra.ridge.RidgeMessage;

public interface OptimisticMergerCallback {
   void uponOptimisticMerge(RidgeMessage message);
}
