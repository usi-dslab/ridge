package ch.usi.dslab.bezerra.ridge.storage;

import ch.usi.dslab.bezerra.ridge.RidgeMessage;

public class NoStorage implements Storage {

   @Override
   public int putInstanceBatch(Long instanceId, RidgeMessage instanceBatch) {
      return 0;
   }

   @Override
   public RidgeMessage get(Long key) {
      return null;
   }

}
