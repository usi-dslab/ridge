package ch.usi.dslab.bezerra.ridge.storage;

import java.util.Iterator;

import ch.usi.dslab.bezerra.ridge.RidgeMessage;

public class ModArrayStorage implements Storage {

   public static class ModArrayBuffer<T> implements Iterable<T> {
      T[] buffer;

      @SuppressWarnings("unchecked")
      public ModArrayBuffer(int capacity) {
         capacity = capacity > 1 ? capacity : 2;
         buffer = (T[]) new Object[capacity];
      }

      public void add(long key, T item) {
         buffer[(int) (key % (long) buffer.length)] = item;
      }

      @Override
      public Iterator<T> iterator() {
         Iterator<T> it = new Iterator<T>() {

            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
               return currentIndex < buffer.length;
            }

            @Override
            public T next() {
               return buffer[currentIndex];
            }

            @Override
            public void remove() {
               throw new UnsupportedOperationException();
            }
         };
         return it;
      }

   }

   ModArrayBuffer<RidgeMessage> instances;

   public ModArrayStorage() {
      instances = new ModArrayBuffer<RidgeMessage>(Storage.CACHE_SIZE);
   }

   @Override
   public int putInstanceBatch(Long instanceId, RidgeMessage instanceBatch) {
      instances.add(instanceId, instanceBatch);
      return 0;
   }

   @Override
   public RidgeMessage get(Long key) {
      for (RidgeMessage batch : instances)
         if (batch != null && batch.getInstanceId() == key)
            return batch;
      return null;
   }

}
