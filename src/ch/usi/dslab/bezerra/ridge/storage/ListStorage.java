package ch.usi.dslab.bezerra.ridge.storage;

import java.util.LinkedList;
import java.util.List;

import ch.usi.dslab.bezerra.ridge.RidgeMessage;

public class ListStorage implements Storage {
   List<RidgeMessage> instances;
   boolean isLimited;
   int limit;
   
   public ListStorage() {
      this(new LinkedList<RidgeMessage>());
   }
   
   public ListStorage(int maxValues) {
      this();
      isLimited = true;
      limit = maxValues;
   }
   
   public ListStorage(List<RidgeMessage> list) {
      instances = list;
      isLimited = false;
   }

   @Override
   public int putInstanceBatch(Long instanceId, RidgeMessage instanceBatch) {
      while(isLimited && limit > 0 && instances.size() >= limit)
         instances.remove(0);
      instances.add(instanceBatch);
      return 0;
   }

   @Override
   public RidgeMessage get(Long key) {
      for (RidgeMessage batch : instances)
         if (batch.getInstanceId() == key)
            return batch;
      return null;
   }

}
