/*

 Ridge - A high-throughput, low-latency amcast library
 Copyright (C) 2015, University of Lugano
 
 This file is part of Ridge.
 
 Ridge is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.ridge.storage;

public class StorageFactory {
   
   public static Storage buildStorage(String dbtype, int uid) {
      if (dbtype == null) return null;
      Storage storage = null;
      switch (dbtype) {
         case "bdbsync" :
         case "bdb" : {
            String path = "/tmp/ridge-bdb/" + uid;
            storage = new BDBStorage(path, true);
            break;
         }
         case "bdbasync" : {
            String path = "/tmp/ridge-bdb/" + uid;
            storage = new BDBStorage(path, false);
            break;
         }
         case "memory" : {
            storage = new MemoryStorage();
            break;
         }
         case "memcache" : {
            storage = new MemCacheStorage();
            break;
         }
         case "listcache" : {
            storage = new ListStorage(Storage.CACHE_SIZE);
            break;
         }
         case "fastarray" : {
            storage = new FastArrayStorage();
            break;
         }
         case "modarray" : {
            storage = new ModArrayStorage();
            break;
         }
         case "nostorage" : {
            storage = new NoStorage();
            break;
         }
         case "nullstorage" : {
            storage = null;
            break;
         }
         default : {
            storage = null;
            break;
         }
      }
      return storage;
   }

}
