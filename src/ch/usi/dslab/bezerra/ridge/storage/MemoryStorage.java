/*

 Ridge - A high-throughput, low-latency amcast library
 Copyright (C) 2015, University of Lugano
 
 This file is part of Ridge.
 
 Ridge is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.ridge.storage;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import ch.usi.dslab.bezerra.ridge.RidgeMessage;

public class MemoryStorage implements Storage {
   
   Map<Long, RidgeMessage> memMap;

   public MemoryStorage() {
      this(new ConcurrentHashMap<Long, RidgeMessage>());
   }
   
   public MemoryStorage(Map<Long, RidgeMessage> map) {
      this.memMap = map;
   }
   
   @Override
   public int putInstanceBatch(Long instanceId, RidgeMessage instanceBatch) {
      memMap.put(instanceId, instanceBatch);
      return 0;
   }

   @Override
   public RidgeMessage get(Long key) {
      return memMap.get(key);
   }

}
