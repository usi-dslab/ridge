/*

 Ridge - A high-throughput, low-latency amcast library
 Copyright (C) 2014, University of Lugano
 
 This file is part of Ridge.
 
 Ridge is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.ridge;

import java.util.ArrayList;
import java.util.List;

public class AcceptorSequence {
   
   

   
   
   /*
    *  INSTANCE MEMBERS
    */
   
   // acceptor sequence id is important to tell the acceptors which route should be taken for that batch
   int id;
   boolean coordinatorWrites;
   List<Acceptor> sequence = new ArrayList<Acceptor>();

   public AcceptorSequence(int id, int ensembleId, boolean coordinatorWrites, Acceptor... acceptors) {
      this.id = id;
      this.coordinatorWrites = coordinatorWrites;
      for (Acceptor acc : acceptors)
         this.sequence.add(acc);
      Ensemble.getEnsemble(ensembleId).addAcceptorSequence(this);
   }
   
   public AcceptorSequence(int id, int paxosGroupId, boolean coordinatorWrites, List<Acceptor> acceptors) {
      this(id, paxosGroupId, coordinatorWrites, acceptors.toArray(new Acceptor[0]));
   }
   
   public void addAcceptor(Acceptor acceptor) {
      if (sequence.contains(acceptor) == false)
         sequence.add(acceptor);
   }
   
   public boolean removeAcceptor(Acceptor acceptor) {
      return sequence.remove(acceptor);
   }

   public Acceptor getNextAcceptor(Acceptor current) {
      int pos = sequence.indexOf(current);
      pos++;
      if (pos < sequence.size())
         return sequence.get(pos);
      else
         return null;
         // a null return at this point means that the _current_ acceptor is the last one.
   }
}
