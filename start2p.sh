#!/usr/bin/env bash

CMD="java -XX:+UseG1GC -cp /Users/longle/Dropbox/Workspace/PhD/DS-SMR/ridge/target/ridge-git.jar -Dlog4j.configuration=file:/Users/longle/Dropbox/Workspace/PhD/DS-SMR/ridge/log4j2.xml"

$CMD ch.usi.dslab.bezerra.sense.DataGatherer 40000 /tmp/ridge throughput learner 6 latency client 1 &

$CMD ch.usi.dslab.bezerra.ridge.Coordinator 0 &

$CMD ch.usi.dslab.bezerra.ridge.Acceptor 1 &
$CMD ch.usi.dslab.bezerra.ridge.Acceptor 2 &

$CMD ch.usi.dslab.bezerra.ridge.Learner 3 &
$CMD ch.usi.dslab.bezerra.ridge.Learner 4 &
$CMD ch.usi.dslab.bezerra.ridge.Learner 5 &

$CMD ch.usi.dslab.bezerra.ridge.Coordinator 6 &

$CMD ch.usi.dslab.bezerra.ridge.Acceptor 7 &
$CMD ch.usi.dslab.bezerra.ridge.Acceptor 8 &

$CMD ch.usi.dslab.bezerra.ridge.Learner 9 &
$CMD ch.usi.dslab.bezerra.ridge.Learner 10 &
$CMD ch.usi.dslab.bezerra.ridge.Learner 11 &

$CMD ch.usi.dslab.bezerra.ridge.Learner 12 &

#$CMD ch.usi.dslab.bezerra.ridge.Client 12 false 0 #interactive
#$CMD ch.usi.dslab.bezerra.ridge.Client 13 false 60 #auto in 20s