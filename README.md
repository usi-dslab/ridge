Ridge

Allows the creation of Paxos ensembles (coordinator plus acceptors), where
learners can learn decisions from multiple such ensembles, using timestamp
(real-time) based deterministic merge, providing:

- atomic delivery

- optimistic (i.e., uniform agreement, with probable atomic order)

- fast (i.e., non-uniform agreement, with probable atomic order)

Besides, Ridge provides maximum theoretical throughput, by routing each
value through a different path among the learners.



This is a fork of [Ridge](https://bitbucket.org/kdubezerra/ridge)